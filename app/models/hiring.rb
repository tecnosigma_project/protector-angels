module Hiring
  STEPS = { identification: I18n.t('hiring_steps.identification'),
            vehicles: I18n.t('hiring_steps.vehicles'),
            resume: I18n.t('hiring_steps.resume'),
            payment: I18n.t('hiring_steps.payment'),
            finalization: I18n.t('hiring_steps.finalization') }
end
