FactoryBot.define do
  factory :bank do
    compe_register { '341' }
    name { 'Banco Itaú S.A.' }
  end
end
