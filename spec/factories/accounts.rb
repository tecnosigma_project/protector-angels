FactoryBot.define do
  factory :account do
    agency { "1234-5" }
    number { "12345678-X" }
  end
end
