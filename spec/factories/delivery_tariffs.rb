FactoryBot.define do
  factory :delivery_tariff do
    federative_unit { 'SP' }
    destiny { 'SAO' }
    kind { ['Capital', 'Interior'].sample }
    kg_1 { 1.5 }
    kg_2 { 1.5 }
    kg_3 { 1.5 }
    kg_4 { 1.5 }
    kg_5 { 1.5 }
    kg_6 { 1.5 }
    kg_7 { 1.5 }
    kg_8 { 1.5 }
    kg_9 { 1.5 }
    kg_10 { 1.5 }
    kg_11 { 1.5 }
    kg_12 { 1.5 }
    kg_13 { 1.5 }
    kg_14 { 1.5 }
    kg_15 { 1.5 }
    kg_16 { 1.5 }
    kg_17 { 1.5 }
    kg_18 { 1.5 }
    kg_19 { 1.5 }
    kg_20 { 1.5 }
    kg_21 { 1.5 }
    kg_22 { 1.5 }
    kg_23 { 1.5 }
    kg_24 { 1.5 }
    kg_25 { 1.5 }
    kg_26 { 1.5 }
    kg_27 { 1.5 }
    kg_28 { 1.5 }
    kg_29 { 1.5 }
    kg_30 { 1.5 }
    additional_kg { 1.5 }
  end
end
